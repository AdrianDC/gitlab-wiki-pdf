# gitlab-wiki-pdf

- **Sources :** [RadianDevCore/tools/gitlab-wiki-pdf](https://gitlab.com/RadianDevCore/tools/gitlab-wiki-pdf)
- **Maintainer :** Adrian DC

---

## Build locally

```bash
gcil
```

---

## Requirements

### Dependencies

- Docker: <https://docs.docker.com/engine/install/>
- gcil: <https://radiandevcore.gitlab.io/tools/gcil>

### Configurations

- ~/.ssh: SSH keys to clone Wiki sources
